using System.Threading;
using System.Threading.Tasks;

namespace MiroBozik.TaskScheduler
{
    /// <summary>
    /// Scheduled Task Interface
    /// </summary>
    public interface IScheduledTask
    {
        /// <summary>
        /// Cron Expression
        /// </summary>
        string Schedule { get; }

        /// <summary>
        /// Execute task
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>instance of <see cref="System.Threading.Tasks.Task"/></returns>
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}