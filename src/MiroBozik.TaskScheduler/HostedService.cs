﻿using System.Threading;
using Microsoft.Extensions.Hosting;

namespace MiroBozik.TaskScheduler
{
    public abstract class HostedService : IHostedService
    {
        private System.Threading.Tasks.Task _executingTask;
        private CancellationTokenSource _cts;

        public System.Threading.Tasks.Task StartAsync(CancellationToken cancellationToken)
        {
            // Create a linked token so we can trigger cancellation outside of this token's cancellation
            _cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

            // Store the task we're executing
            _executingTask = ExecuteAsync(_cts.Token);

            // If the task is completed then return it, otherwise it's running
            return _executingTask.IsCompleted ? _executingTask : System.Threading.Tasks.Task.CompletedTask;
        }

        public async System.Threading.Tasks.Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop called without start
            if (_executingTask == null)
            {
                return;
            }

            // Signal cancellation to the executing method
            _cts.Cancel();

            // Wait until the task completes or the stop token triggers
            await System.Threading.Tasks.Task.WhenAny(_executingTask, System.Threading.Tasks.Task.Delay(-1, cancellationToken));

            // Throw if cancellation triggered
            cancellationToken.ThrowIfCancellationRequested();
        }

        // Derived classes should override this and execute a long running method until
        // cancellation is requested
        protected abstract System.Threading.Tasks.Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
