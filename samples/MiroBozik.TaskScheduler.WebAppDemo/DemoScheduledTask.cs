﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MiroBozik.TaskScheduler.WebAppDemo
{
    public class DemoScheduledTask : IScheduledTask
    {
        public string Schedule { get; } = "* * * * *"; // every minute

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Demo scheduled task run at {0}", DateTime.Now);

            return Task.CompletedTask;
        }
    }
}